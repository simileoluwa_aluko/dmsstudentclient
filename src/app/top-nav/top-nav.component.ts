import { Component, OnInit } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-nav',
  templateUrl: './top-nav.component.html',
  styleUrls: ['./top-nav.component.scss'],
})
export class TopNavComponent implements OnInit {
  regNumber = ''

  constructor(private breakpointObserver: BreakpointObserver, private router: Router) { }

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );

  ngOnInit(){
    let accountDetails = JSON.parse(sessionStorage.getItem('accountDetails'))
    this.regNumber = accountDetails.regNumber
  }

  displayDashboard(){
    this.router.navigate(['/dashboard', {outlets : {'topNav' :['dashboard']}}])
  }

  displayRegistration(){
    this.router.navigate(['/dashboard', {outlets : {'topNav' :['registration']}}])
  }

  logout(){
    this.router.navigate(['/login'])
  }
}
