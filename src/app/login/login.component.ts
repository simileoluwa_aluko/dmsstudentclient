import { Component, OnInit, Injectable } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from '../services/login.service';
import { Router } from '@angular/router';
import { of } from 'rxjs';
import {IloginResponse} from '../interfaces/loginResponseInterface'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
@Injectable({
  providedIn: 'root'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup
  isLoggenIn = of(false)
  loginError: boolean
  loading = false
  token: string
  regNumber: string
  accountDetails : IloginResponse


  constructor(private fb: FormBuilder, private loginService: LoginService, private router: Router) { }

  ngOnInit() {

    this.loginForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    })
  }

  get email() {
    return this.loginForm.get('email')
  }

  get password() {
    return this.loginForm.get('password')
  }

  onSubmit() {
    const email = this.loginForm.get('email').value
    const password = this.loginForm.get('password').value
    this.loginError = false;

    this.loading = true

    this.loginService.login(email, password).then(response => {
      this.loading = false
      if (response.message == 'authentication successful') {
        sessionStorage.setItem('accountDetails', JSON.stringify(response))
        this.isLoggenIn = of(true)
        this.loginError = false
        this.router.navigate(['/dashboard'])
        this.token = response.token
        this.regNumber = response.registrationNumber
        this.loginForm.get('email').setValue('')
        this.loginForm.get('password').setValue('')
      } else {
        this.isLoggenIn = of(false)
        this.loginError = true
        this.router.navigate(['/login'])
        this.loginForm.get('email').setValue('')
        this.loginForm.get('password').setValue('')
      }
    }).catch(error => {
      console.log(error)
      this.isLoggenIn = of(false)
      this.loginError = true
      this.loading = false
      this.router.navigate(['/login'])
      this.loginForm.get('email').setValue('')
      this.loginForm.get('password').setValue('')  
    })
  }
}
