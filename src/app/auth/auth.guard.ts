import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import {Observable, of} from 'rxjs'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot):Promise<boolean> | Observable<boolean> | boolean {
    console.log(this.loginComponent.isLoggenIn)
    if (this.loginComponent.isLoggenIn) { return of(true) }

    this.router.navigate(['/login'])

    return of(false);
  }

  constructor(private loginComponent: LoginComponent, private router: Router) { }
}
