import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FormsService } from '../services/forms.service'
import { ImageRecognitionService } from '../services/image-recognition.service'

@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.scss']
})
export class FormsComponent implements OnInit {

  myForm: FormGroup
  emptyString = ''
  jambResultImgRecognError = ''
  oLevelImgRecognError = ''
  makingJambImgRecognRequest: boolean = false
  makingOlevelImgRecognRequest: boolean = false

  constructor(private fb: FormBuilder, private formsService: FormsService, private imageRecognService: ImageRecognitionService) { }

  ngOnInit() {
    this.myForm = this.fb.group({
      first_name: [this.emptyString, [Validators.required]],
      last_name: [this.emptyString, [Validators.required]],
      other_names: [this.emptyString, [Validators.required]],
      email: [this.emptyString, [Validators.required, Validators.email]],
      dob: [this.emptyString, [Validators.required]],
      place_of_birth: [this.emptyString, [Validators.required]],
      country_of_origin: [this.emptyString, [Validators.required]],
      state_of_origin: [this.emptyString, [Validators.required]],
      local_govt_area: [this.emptyString, [Validators.required]],
      address: [this.emptyString, [Validators.required]],
      phone_number: [this.emptyString, [Validators.required]],
      student_passport_photo: [this.emptyString, [Validators.required]],

      fg1surname: [this.emptyString, [Validators.required]],
      fg1firstname: [this.emptyString, [Validators.required]],
      fg1Email: [this.emptyString, [Validators.required, Validators.email]],
      fg1PhoneNumber1: [this.emptyString, [Validators.required]],
      fg1PhoneNumber2: [this.emptyString],
      fg1address: [this.emptyString, [Validators.required]],
      fg1stateOfOrgin: [this.emptyString, [Validators.required]],
      fg1PassportPhoto: [this.emptyString, [Validators.required]],
      fg1Occupation: [this.emptyString, [Validators.required]],

      mg2surname: [this.emptyString, [Validators.required]],
      mg2firstname: [this.emptyString, [Validators.required]],
      mg2Email: [this.emptyString, [Validators.required, Validators.email]],
      mg2PhoneNumber1: [this.emptyString, [Validators.required]],
      mg2PhoneNumber2: [this.emptyString],
      mg2address: [this.emptyString, [Validators.required]],
      mg2stateOfOrgin: [this.emptyString, [Validators.required]],
      mg2PassportPhoto: [this.emptyString, [Validators.required]],
      mg2Occupation: [this.emptyString, [Validators.required]],

      oLevelResult: [this.emptyString, [Validators.required]],
      oLevelResultFile: [this.emptyString, [Validators.required]],
      jamb_result: [this.emptyString, [Validators.required]],
      jamb_admission_letter: [this.emptyString, [Validators.required]],
      recommendation_letter: [this.emptyString, [Validators.required]],
      birth_cert: [this.emptyString, [Validators.required]]
    })
  }

  onSubmit() {
    (<HTMLInputElement>document.getElementById("submitDocuments")).disabled = true
    let oLevelResultFile = (<HTMLInputElement>document.getElementById('oLevelResult')).files[0]
    let oLevelResult = this.myForm.get('oLevelResult').value
    let accountDetails = JSON.parse(sessionStorage.getItem('accountDetails'))
    let token = accountDetails.token
    let regNumber = accountDetails.regNumber

    let studentPassportPhoto = (<HTMLInputElement>document.getElementById('studentPassportPhoto')).files[0]
    let fathersPassportPhoto = (<HTMLInputElement>document.getElementById('fathersPassportPhoto')).files[0]
    let mothersPassportPhoto = (<HTMLInputElement>document.getElementById('mothersPassportPhoto')).files[0]
    let recommendationLetter = (<HTMLInputElement>document.getElementById('recommendationLetter')).files[0]
    let jambAdmissionLetter = (<HTMLInputElement>document.getElementById('jambAdmissionLetter')).files[0]
    let birthCertificate = (<HTMLInputElement>document.getElementById('birthCertificate')).files[0]
    let jambResult = (<HTMLInputElement>document.getElementById('jambResult')).files[0]



    let firstName = this.first_name.value
    let lastName = this.last_name.value
    let otherNames = this.other_names.value
    let Email = this.email.value
    let dateOfBirth = this.dob.value
    let placeOfBirth = this.place_of_birth.value
    let countryOfOrigin = this.country_of_origin.value
    let stateOfOrigin = this.state_of_origin.value
    let localGovtArea = this.local_govt_area.value
    let Address = this.local_govt_area.value
    let phoneNumber = this.phone_number.value
    let fathersFirstName = this.fg1firstname.value
    let fathersSurname = this.fg1surname.value
    let fathersEmail = this.fg1Email.value
    let fathersPhoneNumberOne = this.fg1PhoneNumber1.value
    let fathersPhoneNumberTwo = this.fg1PhoneNumber2.value
    let fathersAddress = this.fg1address.value
    let fathersStateOfOrigin = this.fg1stateOfOrgin.value
    let fathersOccupation = this.fg1Occupation.value
    let mothersFirstName = this.mg2firstname.value
    let mothersSurname = this.mg2surname.value
    let mothersEmail = this.mg2Email.value
    let mothersPhoneNumberOne = this.mg2PhoneNumber1.value
    let mothersPhoneNumberTwo = this.mg2PhoneNumber2.value
    let mothersAddress = this.mg2address.value
    let mothersStateOfOrigin = this.mg2stateOfOrgin.value
    let mothersOccupation = this.mg2Occupation.value

    let formData = new FormData()
    formData.append('firstName', firstName)
    formData.append('lastName', lastName)
    formData.append('otherNames', otherNames)
    formData.append('Email', Email)
    formData.append('dateOfBirth', dateOfBirth)
    formData.append('placeOfBirth', placeOfBirth)
    formData.append('countryOfOrigin', countryOfOrigin)
    formData.append('stateOfOrigin', stateOfOrigin)
    formData.append('localGovtArea', localGovtArea)
    formData.append('Address', Address)
    formData.append('phoneNumber', phoneNumber)
    formData.append('studentPassportPhoto', studentPassportPhoto)
    formData.append('regNumber', regNumber)
    formData.append('fathersFirstName', fathersFirstName)
    formData.append('fathersSurname', fathersSurname)
    formData.append('fathersEmail', fathersEmail)
    formData.append('fathersPhoneNumberOne', fathersPhoneNumberOne)
    formData.append('fathersPhoneNumberTwo', fathersPhoneNumberTwo)
    formData.append('fathersAddress', fathersAddress)
    formData.append('fathersStateOfOrigin', fathersStateOfOrigin)
    formData.append('fathersPassportPhoto', fathersPassportPhoto)
    formData.append('fathersOccupation', fathersOccupation)
    formData.append('mothersFirstName', mothersFirstName)
    formData.append('mothersSurname', mothersSurname)
    formData.append('mothersEmail', mothersEmail)
    formData.append('mothersPhoneNumberOne', mothersPhoneNumberOne)
    formData.append('mothersPhoneNumberTwo', mothersPhoneNumberTwo)
    formData.append('mothersAddress', mothersAddress)
    formData.append('mothersStateOfOrigin', mothersStateOfOrigin)
    formData.append('mothersPassportPhoto', mothersPassportPhoto)
    formData.append('mothersOccupation', mothersOccupation)
    formData.append('jambResultImage', jambResult)
    formData.append('oLevelResult', oLevelResultFile)
    formData.append('oLevelResultType', oLevelResult)
    formData.append('recommendationLetter', recommendationLetter)
    formData.append('jambAdmissionLetter', jambAdmissionLetter)
    formData.append('birthCertificate', birthCertificate)

    this.formsService.submitData(formData, token).subscribe(response => {
      alert(`Form Submission successful`)
      this.ngOnInit()
    }, error => {
      alert(`Submission of form not successful. contact administrator`)
      console.log(error)
      this.ngOnInit()
    })
  }

  onFileChange($event, formControlName) {
    let file = $event.target.files[0]
    let formcontrolElement = this.myForm.controls[formControlName]
    this.jambResultImgRecognError = ''

    if ((formControlName == 'jamb_result' || formControlName == 'oLevelResultFile') && file != undefined) {
      formcontrolElement.setValue('')
      if (formControlName == 'jamb_result') {
        this.makingJambImgRecognRequest = true
      } else if (formControlName == 'oLevelResultFile') {
        this.makingOlevelImgRecognRequest = true
      }

      let formData = new FormData()
      formData.append('image', file)

      this.imageRecognService.validateImage(formData).subscribe(response => {
        this.makingJambImgRecognRequest = false
        this.makingOlevelImgRecognRequest = false
        
        let payload = response.predictionResult[0].payload[0]
        let predictedName = payload.displayName
        console.log(payload, predictedName)
        if(formControlName == 'jamb_result' && predictedName != 'jamb'){
          this.jambResultImgRecognError = 'You did not upload a JAMB result'
        }else if(formControlName == 'oLevelResultFile' && predictedName != 'waec'){
          this.jambResultImgRecognError = 'You did not upload a WAEC result'
        }else{
          formcontrolElement.setValue(file ? file.name : '')
        }
      }, error => {
        console.log(error)
        this.makingJambImgRecognRequest = false
        this.makingOlevelImgRecognRequest = false

        if (formControlName == 'jamb_result') {
          this.jambResultImgRecognError = 'Error performing image recognition'
        } else if (formControlName == 'oLevelResultFile') {
          this.oLevelImgRecognError = 'Error performing image recognition'
        }
      })
    } else {
      formcontrolElement.setValue(file ? file.name : '')
    }
  }

  get first_name() {
    return this.myForm.get('first_name')
  }

  get last_name() {
    return this.myForm.get('last_name')
  }

  get other_names() {
    return this.myForm.get('other_names')
  }

  get email() {
    return this.myForm.get('email')
  }

  get dob() {
    return this.myForm.get('dob')
  }

  get place_of_birth() {
    return this.myForm.get('place_of_birth')
  }
  get country_of_origin() {
    return this.myForm.get('country_of_origin')
  }
  get state_of_origin() {
    return this.myForm.get('state_of_origin')
  }
  get local_govt_area() {
    return this.myForm.get('local_govt_area')
  }
  get address() {
    return this.myForm.get('address')
  }
  get phone_number() {
    return this.myForm.get('phone_number')
  }
  get student_passport_photo() {
    return this.myForm.get('student_passport_photo')
  }
  get fg1surname() {
    return this.myForm.get('fg1surname')
  }
  get fg1firstname() {
    return this.myForm.get('fg1firstname')
  }
  get fg1Email() {
    return this.myForm.get('fg1Email')
  }
  get fg1PhoneNumber1() {
    return this.myForm.get('fg1PhoneNumber1')
  }
  get fg1PhoneNumber2() {
    return this.myForm.get('fg1PhoneNumber2')
  }
  get fg1address() {
    return this.myForm.get('fg1address')
  }
  get fg1stateOfOrgin() {
    return this.myForm.get('fg1stateOfOrgin')
  }

  get mg2surname() {
    return this.myForm.get('mg2surname')
  }
  get mg2firstname() {
    return this.myForm.get('mg2firstname')
  }
  get mg2Email() {
    return this.myForm.get('mg2Email')
  }
  get mg2PhoneNumber1() {
    return this.myForm.get('mg2PhoneNumber1')
  }
  get mg2PhoneNumber2() {
    return this.myForm.get('mg2PhoneNumber2')
  }
  get mg2address() {
    return this.myForm.get('mg2address')
  }
  get mg2stateOfOrgin() {
    return this.myForm.get('mg2stateOfOrgin')
  }

  get mg2PassportPhoto() {
    return this.myForm.get('mg2PassportPhoto')
  }

  get mg2Occupation() {
    return this.myForm.get('mg2Occupation')
  }

  get oLevelResultFile() {
    return this.myForm.get('oLevelResultFile')
  }
  get jamb_result() {
    return this.myForm.get('jamb_result')
  }
  get jamb_admission_letter() {
    return this.myForm.get('jamb_admission_letter')
  }
  get recommendation_letter() {
    return this.myForm.get('recommendation_letter')
  }
  get birth_cert() {
    return this.myForm.get('birth_cert')
  }

  get fg1PassportPhoto() {
    return this.myForm.get('fg1PassportPhoto')
  }
  get fg1Occupation() {
    return this.myForm.get('fg1Occupation')
  }

  get oLevelResult() {
    return this.myForm.get('oLevelResult')
  }

}
