export interface IloginResponse {
    message: string;
    registrationNumber?: string;
    token?: string;
    _id?: string;
    firstName?: string;
    lastName?: string;
    email?: string;
    course?: string;
    college?: string;
    regNumber?: string;
    admissionYear?: string;
    department?: string;
    admissionStatus?: string;
    studentAvatar?: string;
    informationCompletionStatus?: number,

}