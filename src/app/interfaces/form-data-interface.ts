export interface FormDataInterface {
    firstName: string;
    lastName: string;
    otherNames: string;
    alternativeEmail: string;
    dateOfBirth: string;
    placeOfBirth: string;
    countryOfOrigin: string;
    stateOfOrigin: string;
    localGovtArea: string;
    Address: string;
    phoneNumber: string;
    studentPassportPhoto: string;
    regNumber : string;
    fathersFirstName: string;
    fathersSurname: string;
    fathersEmail: string;
    fathersPhoneNumberOne: string;
    fathersPhoneNumberTwo: string;
    fathersAddress: string;
    fathersStateOfOrigin: string;
    fathersPassportPhoto: string;
    fathersOccupation: string;
    mothersFirstName: string;
    mothersSurname: string;
    mothersEmail: string;
    mothersPhoneNumberOne: string;
    mothersPhoneNumberTwo: string;
    mothersAddress: string;
    mothersStateOfOrigin: string;
    mothersPassportPhoto: string;
    mothersOccupation: string;

    //documents
    jambResultImage: string;
    waecResultImage: string;
    igcseImage: string;
    nabtebImage: string;
    necoResultImage: string;
    recommendationLetter: string;
    jambAdmissionLetter: string;
    birthCertificate: string;
}
