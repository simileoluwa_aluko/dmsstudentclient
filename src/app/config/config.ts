export const port = 8000
const baseUrl = `http://localhost:${port}`

export const Config = {
    loginRoute : `${baseUrl}/accounts/login/`,
    dashboardRoute : `${baseUrl}accounts/`,
    submitForm : `${baseUrl}/students/`,
    imageRecognRoute : `http://localhost:4001/image-recognition/`
}