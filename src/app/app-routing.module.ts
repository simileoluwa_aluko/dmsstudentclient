import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormsComponent } from './forms/forms.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { TopNavComponent } from './top-nav/top-nav.component';
import {AuthGuard} from '../app/auth/auth.guard'


const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: TopNavComponent,
    canActivate : [AuthGuard],
    children: [
      {
        path: '',
        pathMatch : 'full',
        canActivate : [AuthGuard],
        component : DashboardComponent,
        outlet : 'topNav'
      },
      {
        path : 'registration',
        canActivate : [AuthGuard],
        component : FormsComponent,
        outlet : 'topNav'
      },
      {
        path : 'dashboard',
        canActivate : [AuthGuard],
        component : DashboardComponent,
        outlet : 'topNav'
      }
    ]
  },
  {
    path: 'login',
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
