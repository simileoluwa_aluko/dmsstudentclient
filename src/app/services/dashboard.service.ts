import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(private http : HttpClient) { }

  getDashboardData(accountId : string, token : string){
    let dashboardUrl = Config.Config.dashboardRoute

    return this.http.get(dashboardUrl, {headers : {'Authorization' : `Bearer ${token}`, 'Content-Type' : 'application/json'}})
  }
}
