import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import {IloginResponse} from '../interfaces/loginResponseInterface'
import * as Config from '../config/config'

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    
    let loginUrl = Config.Config.loginRoute

    let data = {
      email: email,
      password: password
    }
    return this.http.post<IloginResponse>(loginUrl, data, { headers: { 'Content-Type': 'application/json' } }).toPromise()
  }
}
