import { Injectable } from '@angular/core';
import * as Config from '../config/config'
import {HttpClient} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class FormsService {

  constructor(private http : HttpClient) { }

  submitData(formData : FormData, token : string){
    let formSubmitUrl = Config.Config.submitForm
    
    return this.http.post(formSubmitUrl, formData, {headers : {'Authorization' : `Bearer ${token}`}})
  }
}
