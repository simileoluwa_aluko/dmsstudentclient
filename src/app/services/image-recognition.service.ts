import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import {ImageRecognInterface} from '../interfaces/image-recogn-interface'
import * as Config from '../config/config'


@Injectable({
  providedIn: 'root'
})
export class ImageRecognitionService {

  constructor(private http : HttpClient) { }

  validateImage(formData : FormData){
    let imageRecognUrl = Config.Config.imageRecognRoute

    return this.http.post<ImageRecognInterface>(imageRecognUrl, formData)
  }
}
