import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IloginResponse } from '../interfaces/loginResponseInterface';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  studentAvatarPublicId = 'dms/accounts/default_account_img'
  emptyString = ''
  accountDetails : IloginResponse
  name = this.emptyString
  email = this.emptyString
  regNumber = this.emptyString
  department = this.emptyString
  course = this.emptyString
  college = this.emptyString
  admissionYear = this.emptyString
  admissionStatus = this.emptyString
  infoCompletionStatus = 0

  constructor() { }

  ngOnInit() {
    this.accountDetails = JSON.parse(sessionStorage.getItem('accountDetails'))
    this.name = this.accountDetails.firstName + ' ' + this.accountDetails.lastName
    this.email = this.accountDetails.email
    this.regNumber = this.accountDetails.regNumber
    this.department = this.accountDetails.department
    this.course = this.accountDetails.course
    this.college = this.accountDetails.college
    this.admissionYear = this.accountDetails.admissionYear
    this.admissionStatus = this.accountDetails.admissionStatus
    this.infoCompletionStatus = this.accountDetails.informationCompletionStatus
    if(this.accountDetails.studentAvatar){
      this.studentAvatarPublicId = this.accountDetails.studentAvatar
    }
    
  }
}